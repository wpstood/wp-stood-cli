const _path       = require('path');
const shell       = require('shelljs');
const chalk       = require('chalk');
const Clui        = require('clui');
const Spinner     = Clui.Spinner;
const pkg         = require('../package.json');

module.exports = () => {

   console.log( chalk.green('Current Version:') + ' ' + chalk.yellow(pkg.version) );
   const status = new Spinner( chalk.blue('Updating to latest version...') );
   status.start();

   require('simple-git')( _path.join(__dirname, '../') )
     .fetch('origin', 'master', (err, FetchSummary) => {

         if(FetchSummary && FetchSummary.remote) {
            status.message( chalk.blue(FetchSummary.remote) );
         }

         status.stop();
         console.log('');
         console.log(chalk.green('Repository: ') + chalk.yellow(FetchSummary.remote) );
         console.log(chalk.blue(FetchSummary.raw));

         status.start();

         status.message( chalk.blue('Checking out to remote branch') );
     })
     .reset(['--hard', 'origin/master'], (err) => {

         if(err){
            // status.stop();
            console.log( chalk.red('Failed to checkout to remote branch') );
            // status.start();
         }
     })
     .pull('origin', 'master', (err, PullSummary) => {
         
         // status.stop();

         const changed = false;

         if(PullSummary && PullSummary.summary) {
            const summary = PullSummary.summary;
            // console.log(' - changes: ' + chalk.yellow(summary.changes));
            // console.log(' - insertions: ' + chalk.yellow(summary.insertions));
            // console.log(' - deletions: ' + chalk.yellow(summary.deletions));

            if(summary.changes > 0){
               changed = true;
            }
         }

         if( changed ){
            // status.start();

            status.message( chalk.blue('Updating packages..') );

            console.log(_path.join(__dirname, '../'));
            shell.cd( _path.join(__dirname, '../') );
            shell.exec('npm update');
        }

        status.stop();

         
     })
     .exec(() => {
         console.log( chalk.green('\nUpdate finished.\n') );
     });

     
}