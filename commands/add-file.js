const inquirer    = require('inquirer');
const shell       = require('shelljs');
const chalk       = require('chalk');
const extend      = require('extend');
const Clui        = require('clui');
const Spinner     = Clui.Spinner;
const slugify	  = require('slug-generator');
const path        = require('path');
const fs          = require('fs-extra');
const { renderString, renderTemplateFile } = require('template-file');

const getFileDetails = async () => {
	const questions = [
      	{
	        type: 'list',
			name: 'fileType',
			message: 'Type of file:',
			choices: [
				{
					name: 'Module Class',
					value: 'class'
				},
				{
					name: 'Database Class',
					value: 'database'
				},
			],
			default: 'class'
      	},
      	{
	        type: 'text',
	        name: 'className',
	        message: 'Class Name:',
	        validate: function(value) {
	          	if (value.length) {
	            	return true;
	          	} else {
	            	return 'Please enter class name.';
	          	}
	        }
      	},
      	{
	        type: 'text',
	        name: 'package',
	        message: 'Package (parent / final class):'
      	}
    ];

    return inquirer.prompt(questions);
};

const createFile = (options, dir) => {

	let status = new Spinner( chalk.blue('Generating file...'), ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);
   	status.start();

	return ( new Promise((resolve,reject) => {

	   	let _basename = options.className.split(' ').map(function(str){
		  	return str.charAt(0).toUpperCase() + str.slice(1);
		});

		if(options.fileType == 'database'){
			_basename.unshift('DB');
		}

		if(options.package.length){
			_basename.unshift(options.package.toUpperCase());
		}

			let data = {
	        class: {
	           name: _basename.join('_'),
	           version: '1.0.0',
	           author: 'WP Stood <hello@wpstood.io>'
	        }
	    }

		// Replace variables in a file
		renderTemplateFile( path.join(__dirname, '..', '_templates', 'php', '__default_class.txt'), data)
			.then( (renderedString) => {
			   	// console.log(renderedString); 
			   	
			   	if(!dir || (dir === 'undefined') ){
			   		dir = '';
			   	}

			   	let file = path.join( process.cwd(), dir, slugify('class-' + _basename.join('-')) + '.php' );
			   	if ( dir.charAt( 0 ) == '/' ) {
			   		file = path.join( dir, slugify('class-' + _basename.join('-')) + '.php' );
			   	}
			   	
			    // ensure file
			    fs.ensureFile( file );

			   	fs.outputFile(file, renderedString, (err) => {
			      	if(err){
			         	console.log(chalk.red(err));
			      	}
			   	});

			   	status.stop();
			   	resolve();
			});
	}));
};

module.exports = async (dir) => {

  	try {
    	let details = await getFileDetails();
    	await createFile(details, dir);
  	} catch(err) {
      	console.log(err);
  	}
};