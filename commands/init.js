const inquirer    = require('inquirer');
const shell       = require('shelljs');
const chalk       = require('chalk');
const extend      = require('extend');
const Clui        = require('clui');
const Spinner     = Clui.Spinner;
const path        = require('path');
const fs          = require('fs-extra');
const { renderString, renderTemplateFile } = require('template-file');


const ignoreGit = [
   '.DS_Store',
   'node_modules/'
];

const npmDependencies = [
   'gulp',
   'gulp-clean-css',
   'gulp-concat',
   'gulp-header',
   'gulp-notify',
   'gulp-plumber',
   'gulp-sass',
   'gulp-sourcemaps',
   'gulp-uglify',
   'merge-stream'
];

const composerDependencies = [
   {
      module: 'cpt',
      package: 'gizburdt/cuztom'
   },
   {
      module: 'http',
      package: 'guzzlehttp/guzzle'
   }
];

const askProjectDetails = (initType) => {

   let typeOptions = [{
         'name': 'Theme',
         'value' : 'theme'
      },
      {
         'name': 'Plugin',
         'value' : 'plugin'
      },
      {
         'name': 'MU Plugin',
         'value' : 'mu-plugin'
      }
   ];

   let questions = [
      {
         name: 'projectName',
         type: 'input',
         message: 'Enter project name:',
         validate: function( value ) {
            if (value.length) {
               return true;
            } else {
               return 'Please enter project name.';
            }
        }
      },
      {
         type: 'list',
         name: 'projectType',
         message: 'Select type of project:',
         choices: typeOptions,
         default: 'theme',
         when: function (response) {

            if(initType === undefined ){
               return true;
            }

            for(var i = 0; i < typeOptions.length; i++) {
               if (typeOptions[i].value == initType) {
                    return false;
               }
            }

            return true;
         }
      },
      {
         name: 'useGulp',
         type: 'confirm',
         message: 'Use Gulp:'
      },
      {
         name: 'useGitFtp',
         type: 'confirm',
         message: 'Deploy with Git Ftp:'
      },
      {
         name: 'hasStaging',
         type: 'confirm',
         message: 'Has Staging:',
         when: function (response) {
            return response.useGitFtp;
         }
      },
      {
         name: 'ftpUrl',
         type: 'input',
         message: 'FTP Url (live):',
         validate: function( value ) {
            if (value.length) {
               return true;
            } else {
               return 'Please enter ftp url for live site.';
            }
         },
         when: function (response) {
            return response.useGitFtp;
         }
      },
      {
         name: 'stagingFtpUrl',
         type: 'input',
         message: 'FTP Url (staging):',
         validate: function( value ) {
            if (value.length) {
               return true;
            } else {
               return 'Please enter ftp url for staging.';
            }
         },
         when: function (response) {
            return response.useGitFtp && response.hasStaging;
         }
      },
      {
         type: 'checkbox',
         name: 'modules',
         message: 'Select modules:',
         choices: [
            {
               'name': 'Settings',
               'value' : 'settings'
            },
            {
               'name': 'Database',
               'value' : 'database'
            },
            {
               'name': 'Custom Post Types / Taxonomies',
               'value' : 'cpt'
            },
            {
               'name': 'Http / Curl',
               'value' : 'http'
            }
         ],
         default: ['settings']
      }
   ];

   return inquirer.prompt(questions);
}

/**
 * Init mu-plugin files
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
const initMuPluginFiles = (options, status) => {

};

/**
 * Init plugin files
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
const initPluginFiles = (options, status) => {
   // fs.ensureDirSync(file);
  
};

/**
 * Init theme files
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
const initThemeFiles = (options, status) => {

   status.message('Generating files and folders');

   console.log( path.join(process.cwd(), '../') );

   // fs.ensureDir(  )
   // .then(() => {
   //    console.log('success!');
   // })
   // .catch(err => {
   //    console.error(err);
   // });
};

const logWithTab = (log, color) => {
   // console.log('    ' + log );
   let arr = String(log).replace(/\r\n/g, "\r").replace(/\n/g, "\r").split(/\r/);
   for (var i in arr)
        console.log('    ' + chalk[color](arr[i]) );
};



const generateFiles = async (options) => {
   
   let status = new Spinner( chalk.blue('Generating files...'), ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);
   status.start();

   let dirs = [
      'assets/css',
      'assets/js',
      'includes',
   ];

   let files = [];

   if( options.useGulp ){
      files.push('scss/main.scss');
      files.push('scripts/main.js');
   } else {
      files.push('assets/css/' + options.projectName + '-' + options.projectType + '.css');
      files.push('assets/js/' + options.projectName + '-' + options.projectType + '.js');
   }

   for (let i = 0; i < dirs.length; i++) { 
      fs.ensureDir( path.join( process.cwd(), dirs[i] ) );
   }

   for (let i = 0; i < files.length; i++) { 
      fs.ensureFile( path.join( process.cwd(), files[i] ) );
   }

   fs.outputFile(path.join( process.cwd(), '.gitignore' ), ignoreGit.join('\n'), (err) => {
      if(err){
         console.log(chalk.red(err));
      }
   });

   if(options.useGulp){
      let obj = {
         name: options.projectName,
         version: options.projectVersion,
         description: options.projectDescription,
         main: 'index.js',
         repository: options.gitRepo,
         author: 'WPStood <hello@wpstood.io>',
         license: 'ISC'
      };

      fs.writeJson( path.join( process.cwd(), 'package.json' ), obj, { spaces: 2,  } );

      const createGulp = () => ( new Promise((resolve, reject) => {
         const data = {
            theme: {
               name: options.projectName,
               uri: options.projectUri,
               author: 'WPStood <hello@wpstood.io>',
               author_uri: 'https://wpstood.io',
               parent_theme_folder: options.parent_theme_folder || '',
               description: options.projectDescription || '',
               text_domain: options.text_domain || '',
            },
            adjective: 'cool'
         }
      
         // Replace variables in a file
         renderTemplateFile( path.join(__dirname, '..', '_templates', 'js', '__'+options.projectType+'_gulpfile.txt'), data)
           .then( (renderedString) => {
               // console.log(renderedString);

               fs.outputFile(path.join( process.cwd(), 'gulpfile.js' ), renderedString, (err) => {
                  if(err){
                     console.log(chalk.red(err));
                  }
               });

               resolve();
           });
      }))

      await createGulp();
   }

   status.stop();

   console.log(chalk.white('+ Files created.'));
   console.log();
};

const setupYarn = (options) => {

   let status = new Spinner(chalk.blue('Installing npm packages...'), ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);
   status.start()

   return ( new Promise((resolve,reject) => {
      shell.exec('yarn add ' + npmDependencies.join(' '), {silent:true}, function(code, stdout, stderr) {

         status.stop();

         console.log();
         console.log(chalk.white('+ [Done] Installing npm packages:'));
         if(stdout){
            logWithTab(stdout, 'green');
         }
         
         
         if(code){
            console.log( chalk.red('  Error:') );
            logWithTab(code, 'red');
         }

         if(stderr){
            console.log(chalk.yellow('  Warning:'));
            logWithTab(stderr, 'yellow')
         }

         resolve();
      });
   }));
}

const setupModules = (modules, options) => {

   let status = new Spinner(chalk.blue('Installing modules...'), ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);
   status.start();

   return ( new Promise((resolve,reject) => {

      setTimeout(() => { 
      
         status.stop();
         console.log(chalk.white('+ Modules installed.'));
         console.log();
         resolve();
      },1000) 

   }));
}

const setupComposer = (dependencies, options) => {

   let status = new Spinner(chalk.blue('Installing composer dependencies...'), ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);
   status.start();

   return ( new Promise((resolve,reject) => {

      shell.exec('composer require ' + dependencies.join(' '), {silent:true}, function(code, stdout, stderr) {

         status.stop();

         console.log();
         console.log(chalk.white('+ [Done] Installing composer dependencies:'));
         if(stdout){
            logWithTab(stdout, 'green');
         }
         
         if(code){
            console.log( chalk.red('  Error:') );
            logWithTab(code, 'red');
         }

         if(stderr){
            console.log(chalk.yellow('  Warning:'));
            logWithTab(stderr, 'yellow')
         }

         resolve();
      });

   }));
}

const secondPromise = () => ( new Promise((resolve,reject) =>{
   setTimeout(() =>{ 
      
      console.log('b')
      resolve('second Promise')
   },1000) 
}));

module.exports = async (projectType) => {

   let answers = await askProjectDetails(projectType)

   let options = {
      projectName: 'project-name',
      projectUri: 'https://wpstood.io',
      projectDescription: 'WPStood project',
      projectVersion: '1.0.0',
      gitRepo: '',
   };

   extend(true, options, answers);

   console.log();

   await generateFiles(options);
   await setupYarn(options);

   if(options.modules && options.modules.length){

      // composer
      let composer_dep = [];
      let modules = options.modules;

      for(var i = 0; i < composerDependencies.length; i++) {
         let index = modules.indexOf(composerDependencies[i].module);
         if(index > -1) {
            // remove from modules
            modules.splice(index, 1);

            // register as dep
            composer_dep.push(composerDependencies[i].package);
         }
      }

      // console.log(composer_dep);
      // console.log('-----')
      // console.log(modules);

      if(modules.length > 0){
         await setupModules(modules, options);
      }

      if(composer_dep.length > 0){
         await setupComposer(composer_dep, options);
      }
   }
   
   console.log('done');

   // console.log(composer_dep);
   // console.log('-----')

};