#!/usr/bin/env node

const program 		= require('commander');
const chalk       	= require('chalk');
const clear       	= require('clear');
const figlet      	= require('figlet');
const pkg         	= require('./package.json');

// commands
const initProject 	= require('./commands/init');
const addFile 		= require('./commands/add-file.js');
const updateCLI 	= require('./commands/update-cli');

program
	.version( pkg.version )
	.usage('[command] [options]')
	.description( pkg.description )
	.option('-p, --path', 'Path information')
	.on('--help', function() {

		const getGithubToken = async () => {
			require('simple-git')( __dirname )
		   		.listRemote(['--get-url'], (err, data) => {
			        if (!err) {
			            console.log('Remote url for repository at ' + __dirname + ':');
			            console.log(data);
			            return data;
			        }
			    });
		}

	    console.log();
	    console.log('  Info:');
	    console.log();
	   	console.log('    Version:       %s', pkg.version );
	   	console.log('    Path:          %s', __dirname );
	    console.log();
	});

const getHeader = () => {
	clear();
	console.log(
	   '\n' +
	   chalk.green(
	      figlet.textSync('wpstOOd', { 
	         font: 'ogre' 
	      })
	   ) + 'v' + pkg.version + ' \n'
	);
}

program 
	.command('init [type]')
	.description('Init project and setup build environment.')
	.action((type) => {
			getHeader();
			initProject(type);
	})
	.on('--help', function() {
	    console.log();
	    console.log('  Examples:');
	    console.log();
	    console.log('    $ wpstood init');
	    console.log('    $ wpstood init theme');
	    console.log('    $ wpstood init plugin');
	    console.log();
  	});

program 
	.command('generate [dir]')
  	.alias('addfile')
	.description('Add file from template.')
	.action((dir) => {
		getHeader();
		addFile(dir);
	})
	.on('--help', function() {
	    console.log();
	    console.log('  Examples:');
	    console.log();
	    console.log('    $ wpstood generate');
	    console.log('    $ wpstood generate mu-plugins/project/includes');
	    console.log();
  	});

program 
	.command('self-update')
	.description('Update CLI to latest version.')
	.action(() => {
			getHeader();
			updateCLI();
	});

if (!process.argv.slice(2).length) {
  	getHeader();
	program.outputHelp();
}

program.parse(process.argv);