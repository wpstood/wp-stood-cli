<?php
/**
 * {{class.name}} Class.
 *
 * @class       {{class.name}}
 * @version		{{class.version}}
 * @author 		{{class.author}}
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * {{class.name}} class.
 */
class {{class.name}} {

    /**
     * Singleton method
     *
     * @return self
     */
    public static function init() {
        static $instance = false;

        if ( ! $instance ) {
            $instance = new {{class.name}}();
        }

        return $instance;
    }

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->includes();
	}
	
	public function includes() {

	}

}

{{class.name}}::init();